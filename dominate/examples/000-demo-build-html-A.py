'''
Demonstration of building complete document
'''
from dominate.tags import html, body, h1


def build_html():
    return html(body(h1('Hello, World!')))


def main():
    print build_html()

if __name__ == '__main__':
    main()
