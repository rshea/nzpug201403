'''
Demonstration of building complete document
'''
from dominate.document import document
from dominate.tags import h1, p, link


def build_document():

    d = document(title="NZPUG Demo - March 2014")

    d.head += link(rel="stylesheet", href="css/blueprint/screen.css",
                   type="text/css", media="screen, projection")
    d.head += link(rel="stylesheet", href="css/blueprint/print.css",
                   type="text/css",  media="print")

    d += h1('Hello, Wellington NZPUG!')
    d += p('This is a paragraph.')

    return d


def main():
    print build_document()

if __name__ == '__main__':
    main()
