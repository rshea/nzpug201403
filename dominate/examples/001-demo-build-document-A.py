'''
Demonstration of building complete document
'''
from dominate.document import document


def build_document():
    d = document(title="NZPUG Demo - March 2014")
    return d


def main():
    print build_document()

if __name__ == '__main__':
    main()
