'''
Demonstration of using decorators to create DOM elements
'''
import os

from dominate.document import document
from dominate.tags import h1, p, link, div

from demoutil import random_str_multiple


def build_links(d):
    '''
    Take the `document` object and add some links to
    style sheets we want available when the page is
    served
    '''
    d.head += link(rel="stylesheet", href="css/blueprint/screen.css",
                   type="text/css", media="screen, projection")
    d.head += link(rel="stylesheet", href="css/blueprint/print.css",
                   type="text/css",  media="print")
    d.head += link(rel="stylesheet", href="css/dmnt_demo_base0.css",
                   type="text/css", media="screen, projection")
    return d


@div(cls='header span-24')
def header():
    h1('Hello, Wellington NZPUG!')
    p("Output of : " + os.path.basename(__file__),
      cls="smalltext",
      style="text-decoration: underline")


@div(cls='content span-24')
def content():
    with div(id='left', cls='span-10'):
        p(random_str_multiple(100))
    with div(id='right', cls='span-10 last'):
        p(random_str_multiple(100))


@div(cls='footer span-24')
def footer():
    p("This is the footer", cls="smalltext")


def build_document():

    d = document(title="NZPUG Demo - March 2014")
    d = build_links(d)
    with d.add(div(cls='container')):
        header()
        content()
        footer()

    return d


def main():
    print build_document()

if __name__ == '__main__':
    main()
