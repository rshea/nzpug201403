'''
Some functions which are common a number of the demos
and which aren't anything directly to do with the use
of dominate
'''
import random
import string


def random_str_multiple(word_count):
    '''
    Generate `word_count` random strings
    '''
    l = []
    for i in range(word_count):
        l.append(random_str(random.randrange(2, 7)))
    return ' '.join(l)


def random_str(letter_count):
    '''
    Generate a random string of length `letter_count`
    '''
    return "".join([random.choice(string.ascii_lowercase)
                   for x in xrange(letter_count)])
