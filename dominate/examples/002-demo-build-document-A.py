'''
Demonstration of building complete document
'''
from dominate.document import document
from dominate.tags import h1, p


def build_document():
    d = document(title="NZPUG Demo - March 2014")
    d += h1('Hello, Wellington NZPUG!')
    d += p('This is a paragraph.')
    return d


def main():
    print build_document()

if __name__ == '__main__':
    main()
