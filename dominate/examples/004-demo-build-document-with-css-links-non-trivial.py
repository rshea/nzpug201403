'''
Demonstration of building complete document
'''
from dominate.document import document
from dominate.tags import h1, p, link, div


def build_links(d):
    '''
    Take the `document` object and add some links to
    style sheets we want available when the page is
    served
    '''
    d.head += link(rel="stylesheet", href="css/blueprint/screen.css",
                   type="text/css", media="screen, projection")
    d.head += link(rel="stylesheet", href="css/blueprint/print.css",
                   type="text/css",  media="print")
    d.head += link(rel="stylesheet", href="css/dmnt_demo_base0.css",
                   type="text/css", media="screen, projection")
    return d


def build_content(d):
    '''
    Take the `document` object build some nested divs and
    add some trivial content
    '''
    with d.add(div(cls='container')):
        with div(id='content', cls='span-24'):
            h1('Hello, Wellington NZPUG!')
            p('This is a paragraph.')
    return d


def build_document():

    d = document(title="NZPUG Demo - March 2014")
    d = build_links(d)
    d = build_content(d)

    return d


def main():
    print build_document()

if __name__ == '__main__':
    main()
